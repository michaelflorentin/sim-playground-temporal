package temporal.detail;

using temporal.detail.Exp;
using temporal.detail.Formula;
using temporal.detail.Unification;

class VerifyEventLog {

    static public function verify<TInfo>(
        e : FormulaFunctor<TInfo>, 
        guards : Array<ExpFunctor<TInfo>>, 
        log : Array<ExpFunctor<TInfo>>,
        u : Unification<TInfo>,
        funs : Externals<TInfo>,
        logError : TInfo->String->Void
    ) : Bool {
        var uClone = u.clone();
        var subst = new Map<String, String>();
        var e1 = u.instantiateFormulaWorker(e, subst);
        var guards1 = guards.map(function(e) return u.instantiateExpWorker(e, subst));
        return if (verifyWorker(e1, log, 0, u)) {
            u.commit(uClone);
            true;
        } else false;
    }

    static private function verifyWorker<TInfo>(
        e : FormulaFunctor<TInfo>, 
        log : Array<ExpFunctor<TInfo>>, i : Int,
        u : Unification<TInfo>
    ) : Bool {
        switch (e.value) {
            case Exp(exp): return u.unifyDirty(exp, log[i]);
            case Always(e1):
                for (j in i...log.length) {
                    if (!verifyWorker(e1, log, j, u)) {
                        return false;
                    }
                }
                return true;
            case RAlways(e1):
                for (j in 0...i) {
                    if (!verifyWorker(e1, log, j, u)) {
                        return false;
                    }
                }
                return true;
            case Eventually(e1):
                for (j in i...log.length) {
                    if (verifyWorker(e1, log, j, u)) {
                        return true;
                    }
                }
                return false;
            case REventually(e1):
                for (j in 0...i) {
                    if (verifyWorker(e1, log, j, u)) {
                        return true;
                    }
                }
                return false;
            case Next(e1):
                return if (i + 1 < log.length) {
                    verifyWorker(e1, log, i + 1, u);
                } else false;
            case RNext(e1):
                return if (0 < i) {
                    verifyWorker(e1, log, i - 1, u);
                } else false;
            case And(e1, e2): 
                return verifyWorker(e1, log, i, u)
                    && verifyWorker(e2, log, i, u);
            case Or(e1, e2): 
                return verifyWorker(e1, log, i, u)
                    || verifyWorker(e2, log, i, u);
            case Imply(e1, e2): 
                return !verifyWorker(e1, log, i, u)
                     || verifyWorker(e2, log, i, u);
            case Not(e1): 
                return !verifyWorker(e1, log, i, u);
        }   
        
    }

    static private function checkGuards<TInfo>(
        guards : Array<ExpFunctor<TInfo>>,
        u : Unification<TInfo>,
        funs : Externals<TInfo>,
        logError : TInfo->String->Void
    ) : Bool {
        var good = true;
        for (guard in guards) {
            var value = guard.reduce(u, funs, logError);
            if (value.isBool()) {
                if (!value.getBool()) {
                    good = false;
                }
            } else {
                logError(guard.meta, "Expected guard to evaluate to a boolean.");
                good = false;
            }
        }
        return good;
    }
    
}