package temporal.detail;

using simpleutil.MapUtils;
using simpleutil.MathUtils;
using temporal.detail.Exp;
using temporal.detail.Formula;

class Unification<TInfo> {
    static public function empty<TInfo>() : Unification<TInfo> {
        return new Unification(
            new Map<String, ExpFunctor<TInfo>>(),
            new Map<String, Int>()
        );
    }

    private function new(
        unification : Map<String, ExpFunctor<TInfo>>,
        vars : Map<String, Int>
    ) {
        this.unification = unification;
        this.vars = vars;
    }

    public function fresh(x : String) : String {
        var v = vars.get(x);
        return if (v == null) {
            vars.set(x, 1);
            x;
        } else {
            vars.set(x, v + 1);
            '${x}_$v';
        }
    }

    public function instantiateFormula(e : FormulaFunctor<TInfo>) : FormulaFunctor<TInfo> {
        var vars = new Map<String, String>();
        return instantiateFormulaWorker(e, vars);
    }

    public function instantiateExp(e : ExpFunctor<TInfo>) : ExpFunctor<TInfo> {
        var vars = new Map<String, String>();
        return instantiateExpWorker(e, vars);
    }

    public function instantiateFormulaWorker(
        e : FormulaFunctor<TInfo>,
        vars : Map<String, String>
    ) : FormulaFunctor<TInfo> {
        return switch (e.value) {
            case Exp(e2): 
                new FormulaFunctor(Exp(
                    instantiateExpWorker(e2, vars)
                ), e.meta);
            case Always(e2):
                new FormulaFunctor(Always(
                    instantiateFormulaWorker(e2, vars)
                ), e.meta);
            case RAlways(e2):
                new FormulaFunctor(RAlways(
                    instantiateFormulaWorker(e2, vars)
                ), e.meta);
            case Eventually(e2):
                new FormulaFunctor(Eventually(
                    instantiateFormulaWorker(e2, vars)
                ), e.meta);
            case REventually(e2):
                new FormulaFunctor(REventually(
                    instantiateFormulaWorker(e2, vars)
                ), e.meta);
            case Next(e2):
                new FormulaFunctor(Next(
                    instantiateFormulaWorker(e2, vars)
                ), e.meta);
            case RNext(e2):
                new FormulaFunctor(RNext(
                    instantiateFormulaWorker(e2, vars)
                ), e.meta);
            case And(e1, e2):
                new FormulaFunctor(And(
                    instantiateFormulaWorker(e1, vars),
                    instantiateFormulaWorker(e2, vars)
                ), e.meta);
            case Or(e1, e2):
                new FormulaFunctor(Or(
                    instantiateFormulaWorker(e1, vars),
                    instantiateFormulaWorker(e2, vars)
                ), e.meta);
            case Imply(e1, e2):
                new FormulaFunctor(Imply(
                    instantiateFormulaWorker(e1, vars),
                    instantiateFormulaWorker(e2, vars)
                ), e.meta);
            case Not(e2):
                new FormulaFunctor(Not(
                    instantiateFormulaWorker(e2, vars)
                ), e.meta);
        }
    }

    public function instantiateExpWorker(
        e : ExpFunctor<TInfo>,
        vars : Map<String, String>
    ) : ExpFunctor<TInfo> {
        return switch (e.value) {
            case Var(x):
                var x1 = vars.get(x);
                if (x1 == null) {
                    x1 = fresh(x);
                    vars.set(x, x1);
                }
                new ExpFunctor(Var(x1), e.meta);
            case String(_): e;
            case Int(_): e;
            case Float(_): e;
            case Bool(_): e;
            case Node(n, cs):
                var cs1 = cs.map(function(c) return instantiateExpWorker(c, vars));
                new ExpFunctor(Node(n, cs1), e.meta);
        }
    }

    public function clone() : Unification<TInfo> {
        return new Unification(unification.copySpineString(), vars.copySpineString());
    }
    public function commit(other : Unification<TInfo>) : Void {
        this.unification = other.unification;
    }

    public function get(x : String) : Null<ExpFunctor<TInfo>> {
        var e = unification.get(x);
        return if (e != null) {
            var e1 = follow(e);
            unification.set(x, e1);
            e1;
        } else null;
    }
    public function followShallow(e : ExpFunctor<TInfo>) : ExpFunctor<TInfo> {
        return switch (e.value) {
            case Var(x): 
                var e1 = unification.get(x);
                if (e1 != null) {
                    var e2 = followShallow(e1);
                    unification.set(x, e2);
                    e2;
                } else e;
            default: e;
        }
    }
    public function follow(e : ExpFunctor<TInfo>) : ExpFunctor<TInfo> {
        return switch (e.value) {
            case Var(x): 
                var e1 = unification.get(x);
                if (e1 != null) {
                    var e2 = follow(e1);
                    unification.set(x, e2);
                    e2;
                } else e;
            case String(_): e;
            case Int(_): e;
            case Float(_): e;
            case Bool(_): e;
            case Node(n, cs): new ExpFunctor(
                Node(n, cs.map(follow)),
                e.meta
            );
        }
    }

    public function unify(e1 : ExpFunctor<TInfo>, e2 : ExpFunctor<TInfo>) : Bool {
        var copy = clone();
        return if (copy.unifyDirty(e1, e2)) {
            commit(copy);
            true;
        } else false;
    }

    // Leaves unification in a bad state on false.
    //TODO: cycle test
    public function unifyDirty(e1 : ExpFunctor<TInfo>, e2 : ExpFunctor<TInfo>) : Bool {
        var fe1 = followShallow(e1);
        var fe2 = followShallow(e2);
        return switch (fe1.value) {
            case Var(x1): switch (fe2.value) {
                case Var(x2): 
                    if (x1 != x2) {
                        unification.set(x1, fe2);
                    }
                    true;
                default: 
                    unification.set(x1, fe2);
                    true;
            }
            case String(v1): switch (fe2.value) {
                case String(v2): v1 == v2;
                case Var(x2): 
                    unification.set(x2, fe1);
                    true;
                default: false;
            }
            case Int(v1): switch (fe2.value) {
                case Int(v2): v1 == v2;
                case Var(x2): 
                    unification.set(x2, fe1);
                    true;
                default: false;
            }
            case Float(v1): switch (fe2.value) {
                case Float(v2): v1.close(v2);
                case Var(x2): 
                    unification.set(x2, fe1);
                    true;
                default: false;
            }
            case Bool(v1): switch (fe2.value) {
                case Bool(v2): v1 == v2;
                case Var(x2): 
                    unification.set(x2, fe1);
                    true;
                default: false;
            }
            case Node(n1, cs1): switch (fe2.value) {
                case Node(n2, cs2):
                    if (n1 == n2) unifysDirty(cs1, cs2)
                    else false;
                case Var(x2): 
                    unification.set(x2, fe1);
                    true;
                default: false;
            }
        }
    }

    public function unifysDirty(es1 : Array<ExpFunctor<TInfo>>, es2 : Array<ExpFunctor<TInfo>>) : Bool {
        if (es1.length == es2.length) {
            for (i in 0...es1.length) {
                if (!unifyDirty(es1[i], es2[i])) {
                    return false;
                }
            }
            return true;
        } else return false;
    }



    private var unification : Map<String, ExpFunctor<TInfo>>;
    private var vars : Map<String, Int>;
}
