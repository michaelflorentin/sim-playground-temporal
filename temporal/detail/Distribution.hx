package temporal.detail;


enum DistributionBase<T, TSelf> {
    Values(xs : Array<T>);
    Generator(gen : Iterable<T>);
    Product(ds : Array<TSelf>);
    
}

class Distribution {
    // Returns a number in the [lo, hi] range.
    static public function rangeInt(lo : Int, hi : Int, r : Void->Float) : Void->Int {
        return if (lo <= hi) 
            function() return lo + Std.int((hi - lo + 1) * r())
        else
            function() return lo;
    }

    // Returns 

    static public rangeFloat(lo : Float, hi : Float, r : Void->Float) : Void->Float {
        return if (lo <= hi)
            function() return lo + (hi - lo) * r();
        else
            function() return lo;
    }



}