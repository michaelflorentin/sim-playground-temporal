package temporal.detail;

using simpleutil.Annotate;
using stringskip.StringPos;
import temporal.detail.Exp;

typedef FormulaSource = FormulaFunctor<StringPos>;
typedef FormulaFunctor<TInfo> = Annotate<FormulaKind<
    FormulaFunctor<TInfo>,
    ExpFunctor<TInfo>
>, TInfo>;

enum FormulaKind<TSelf, TExp> {
    Exp(e : TExp);
    //ALL(i, j where i = j +1).
    //Forall(vars : Array<String>, guards : Array<TExp>, body : TSelf);
    Always(e : TSelf);
    RAlways(e : TSelf);
    Eventually(e : TSelf);
    REventually(e : TSelf);
    Next(e : TSelf);
    RNext(e : TSelf);
    And(e1 : TSelf, e2 : TSelf);
    Or(e1 : TSelf, e2 : TSelf);
    Imply(e1 : TSelf, e2 : TSelf);
    Not(e : TSelf);
}

class FormulaUtils {

    static public function show<TInfo>(e : FormulaFunctor<TInfo>) : String {
        return showImply(e);
    }

    static public function showDeep<TInfo>(
        e : FormulaFunctor<TInfo>, 
        u : Unification<TInfo>
    ) : String {
        return showDeepImply(e, u);
    }
    static public function showDeepC<TInfo>(
        u : Unification<TInfo>
    ) : FormulaFunctor<TInfo>->String {
        return function(e) return showDeep(e, u);
    }


    static private function showImply<TInfo>(e : FormulaFunctor<TInfo>) : String {
        return switch (e.value) {
            case Imply(e1, e2): showOr(e1) + " => " + showImply(e2);
            default: showOr(e);
        }
    }
    static private function showOr<TInfo>(e : FormulaFunctor<TInfo>) : String {
        return switch (e.value) {
            case Or(e1, e2): showOr(e1) + " | " + showOr(e2);
            default: showAnd(e);
        }
    }
    static private function showAnd<TInfo>(e : FormulaFunctor<TInfo>) : String {
        return switch (e.value) {
            case And(e1, e2): showAnd(e1) + " & " + showAnd(e2);
            default: showSimple(e);
        }
    }
    static private function showSimple<TInfo>(e : FormulaFunctor<TInfo>) : String {
        return switch (e.value) {
            case Exp(e): ExpUtils.show(e);
            case Always(e): "G" + showSimple(e);
            case RAlways(e): "-G" + showSimple(e);
            case Eventually(e): "F" + showSimple(e);
            case REventually(e): "-F" + showSimple(e);
            case Next(e): "X" + showSimple(e);
            case RNext(e): "-X" + showSimple(e);
            case Not(e): "!" + showSimple(e);
            default: "(" + showImply(e) + ")";
        }
    }


    static private function showDeepImply<TInfo>(
        e : FormulaFunctor<TInfo>, 
        u : Unification<TInfo>
    ) : String {
        return switch (e.value) {
            case Imply(e1, e2): showDeepOr(e1, u) + " => " + showDeepImply(e2, u);
            default: showDeepOr(e, u);
        }
    }
    static private function showDeepOr<TInfo>(
        e : FormulaFunctor<TInfo>, 
        u : Unification<TInfo>
    ) : String {
        return switch (e.value) {
            case Or(e1, e2): showDeepOr(e1, u) + " | " + showDeepOr(e2, u);
            default: showDeepAnd(e, u);
        }
    }
    static private function showDeepAnd<TInfo>(
        e : FormulaFunctor<TInfo>, 
        u : Unification<TInfo>
    ) : String {
        return switch (e.value) {
            case And(e1, e2): showDeepAnd(e1, u) + " & " + showDeepAnd(e2, u);
            default: showDeepSimple(e, u);
        }
    }
    static private function showDeepSimple<TInfo>(
        e : FormulaFunctor<TInfo>, 
        u : Unification<TInfo>
    ) : String {
        return switch (e.value) {
            case Exp(e): ExpUtils.showDeep(e, u);
            case Always(e): "G" + showDeepSimple(e, u);
            case RAlways(e): "-G" + showDeepSimple(e, u);
            case Eventually(e): "F" + showDeepSimple(e, u);
            case REventually(e): "-F" + showDeepSimple(e, u);
            case Next(e): "X" + showDeepSimple(e, u);
            case RNext(e): "-F" + showDeepSimple(e, u);
            case Not(e): "!" + showDeepSimple(e, u);
            default: "(" + showDeepImply(e, u) + ")";
        }
    }

}
