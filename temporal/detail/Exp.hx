package temporal.detail;

using Lambda;
using simpleutil.Annotate;
using simpleutil.HxEscape;
using simpleutil.IteratorUtils;
using stringskip.ParserUtils;
using stringskip.StringPos;
using temporal.detail.Parse;

typedef ExpSource = ExpFunctor<StringPos>;
typedef ExpFunctor<TInfo> = Annotate<ExpKind<ExpFunctor<TInfo>>, TInfo>;

enum ExpKind<TSelf> {
    Var(x : String);
    String(value : String);
    Int(value : Int);
    Float(value : Float);
    Bool(value : Bool);
    Node(name : String, children : Array<TSelf>);
}

typedef Externals<TInfo> = String->(Array<ExpFunctor<TInfo>>->TInfo->(TInfo->String->Void)->ExpFunctor<TInfo>);

class ExpUtils {

    static public function show<TInfo>(e : ExpFunctor<TInfo>) : String {
        return switch (e.value) {
            case Var(x): x;
            case String(v): v.hxEscape();
            case Int(v): '$v';
            case Float(v): '$v';
            case Bool(v): if (v) "true" else "false";
            case Node(n, es): 
                var esString = if (es.length == 0) ""
                else "(" + es.showElements(show, ", ") + ")";
                '$n$esString';
        } 
    }
    static public function showDeep<TInfo>(e : ExpFunctor<TInfo>, u : Unification<TInfo>) : String {
        return switch (u.followShallow(e).value) {
            case Var(x): x;
            case String(v): v.hxEscape();
            case Int(v): '$v';
            case Float(v): '$v';
            case Bool(v): if (v) "true" else "false";
            case Node(n, es): 
                var esString = if (es.length == 0) ""
                else "(" + es.showElements(function(e2) return showDeep(e2, u), ", ") + ")";
                '$n$esString';
        } 
    }
    static public function showDeepC<TInfo>(
        u : Unification<TInfo>
    ) : ExpFunctor<TInfo>->String {
        return function(e) return showDeep(e, u);
    }

    static public function stringToExp(s : String) : ExpSource {
        return namedStringToExp(s, "s");
    }
    static public function namedStringToExp(s : String, name : String) : ExpSource {
        return s.doHxTrimParseTrace(name, Parse.parseExp);
    }
    static public function stringsToExps(ss : Iterable<String>) : Array<ExpSource> {
        return ss.mapi(function(i, s) return namedStringToExp(s, 's_$i')).array();
    }


    static public function isVar<TInfo>(e : ExpFunctor<TInfo>) : Bool {
        return switch (e.value) {
            case Var(_): true;
            default: false;
        }
    }
    static public function getVar<TInfo>(e : ExpFunctor<TInfo>) : String {
        return switch (e.value) {
            case Var(x): x;
            default: throw("not var");
        }
    }

    static public function isFloat<TInfo>(e : ExpFunctor<TInfo>) : Bool {
        return switch (e.value) {
            case Float(_): true;
            default: false;
        }
    }
    static public function getFloat<TInfo>(e : ExpFunctor<TInfo>) : Float {
        return switch (e.value) {
            case Float(v): v;
            default: throw("not float");
        }
    }

    static public function isBool<TInfo>(e : ExpFunctor<TInfo>) : Bool {
        return switch (e.value) {
            case Bool(_): true;
            default: false;
        }
    }
    static public function getBool<TInfo>(e : ExpFunctor<TInfo>) : Bool {
        return switch (e.value) {
            case Bool(v): v;
            default: throw("not bool");
        }
    }


    static public function reduce<TInfo>(
        e : ExpFunctor<TInfo>,
        u : Unification<TInfo>,
        funs : Externals<TInfo>,
        logError : TInfo->String->Void
    ) : ExpFunctor<TInfo> {
        var fe = u.follow(e);
        return switch (fe.value) {
            case Var(_): fe;
            case String(_): fe;
            case Int(_): fe;
            case Float(_): fe;
            case Bool(_): fe;
            case Node(n, cs):
                var vs = cs.map(function(e2) return reduce(e2, u, funs, logError));
                var f = funs(n);
                if (f != null) f(vs, fe.meta, logError);
                else new ExpFunctor(Node(n, vs), fe.meta);
        }
    }


}
