package temporal.detail;

using StringTools;
using simpleutil.CharCategories;
using stringskip.Parse;
using stringskip.Skip;
using stringskip.StringIndex;
using stringskip.ParseHxString;
using temporal.detail.Exp;
using temporal.detail.Formula;

class Parse {
    static public function parseFormula(i : StringIndex) : FormulaSource {
        return parseImplyFormula(i);
    }

    static public function parseExp(i : StringIndex) : ExpSource {
        var p = i.toPos();
        return if (i.indicateHxString()) {
            var s = i.parseHxStringUtf8();
            new ExpSource(String(s), p);
        
        } else if (i.indicateFloat(false)) {
            var v = i.parseFloat(false);
            new ExpSource(Float(v), p);
        
        } else if (i.indicateInt()) {
            var v = i.parseInt();
            new ExpSource(Int(v), p);
        
        } else if (i.indicateName()) {
            var n = i.parseName();
            if (n == "true") {
                new ExpSource(Bool(true), p);
            } else if (n == "false") {
                new ExpSource(Bool(false), p);
            } else if (isVarName(n)) {
                new ExpSource(Var(n), p);
            } else {
                i.skipHxSpace();
                if (i.tryTheChars("(")) {
                    i.skipHxSpace();
                    var es : Array<ExpSource> = [];
                    var first = true;
                    while (i.has() && !i.indicateTheChars(")")) {
                        if (first) {
                            first = false;
                        } else {
                            i.skipTheChars(",");
                            i.skipHxSpace();
                        }
                        es.push(parseExp(i));
                        i.skipHxSpace();
                    }
                    i.skipTheChars(")");
                    new ExpSource(Node(n, es), p);
                } else new ExpSource(Node(n, []), p);
            }

        } else {
            i.expected("a string or a name");
            new ExpSource(Node("?", []), p);
        }
    }

    static public function indicateExp(i : StringIndex) : Bool {
        return i.indicateHxString()
            || i.indicateName();
    }

    static public function isVarName(s : String) : Bool {
        return s.length > 0 && s.fastCodeAt(0).isLower();
    }

    static private function parseImplyFormula(i : StringIndex) : FormulaSource {
        var p = i.toPos();
        var e = parseOrFormula(i);
        i.skipHxSpace();
        return if (i.indicateTheChars("=>")) {
            i.skipTheChars("=>");
            i.skipHxSpace();
            var es = [e, parseOrFormula(i)];
            i.skipHxSpace();
            while (i.tryTheChars("=>")) {
                es.push(parseOrFormula(i));
                i.skipHxSpace();
            }
            var e = es[es.length - 1];
            for (i in 1...es.length) {
                e = new FormulaSource(Imply(es[es.length - i - 1], e), p);
            }
            e;
        } else e;
    }
    static private function parseOrFormula(i : StringIndex) : FormulaSource {
        var p = i.toPos();
        var e = parseAndFormula(i);
        i.skipHxSpace();
        while (i.tryTheChars("|")) {
            e = new FormulaSource(Or(e, parseAndFormula(i)), p);
            i.skipHxSpace();
        }
        return e;
    }
    static private function parseAndFormula(i : StringIndex) : FormulaSource {
        var p = i.toPos();
        var e = parseSimpleFormula(i);
        i.skipHxSpace();
        while (i.tryTheChars("&")) {
            e = new FormulaSource(And(e, parseSimpleFormula(i)), p);
            i.skipHxSpace();
        }
        return e;
    }
    static private function parseSimpleFormula(i : StringIndex) : FormulaSource {
        var p = i.toPos();
        return if (i.tryTheChars("(")) {
            i.skipHxSpace();
            var e = parseImplyFormula(i);
            i.skipHxSpace();
            i.skipTheChars(")");
            e;
        } else if (i.tryTheChars("G")) {
            i.skipHxSpace();
            var e = parseSimpleFormula(i);
            new FormulaSource(Always(e), p);
        } else if (i.tryTheChars("-G")) {
            i.skipHxSpace();
            var e = parseSimpleFormula(i);
            new FormulaSource(RAlways(e), p);
        } else if (i.tryTheChars("F")) {
            i.skipHxSpace();
            var e = parseSimpleFormula(i);
            new FormulaSource(Eventually(e), p);
        } else if (i.tryTheChars("-F")) {
            i.skipHxSpace();
            var e = parseSimpleFormula(i);
            new FormulaSource(REventually(e), p);
        } else if (i.tryTheChars("X")) {
            i.skipHxSpace();
            var e = parseSimpleFormula(i);
            new FormulaSource(Next(e), p);
        } else if (i.tryTheChars("-X")) {
            i.skipHxSpace();
            var e = parseSimpleFormula(i);
            new FormulaSource(RNext(e), p);
        } else if (i.tryTheChars("!")) {
            i.skipHxSpace();
            var e = parseSimpleFormula(i);
            new FormulaSource(Not(e), p);
        } else if (indicateExp(i)) {
            var e = parseExp(i);
            new FormulaSource(Exp(e), p);
        } else {
            i.expected("'(', 'G', 'F', 'X', !', or a word");
            i.skipToSane();
            new FormulaSource(Exp(new ExpSource(Node("?", []), p)), p);
        }
    }

}
