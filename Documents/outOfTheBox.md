Out of the Box  - 2018/3/23
===========================

Motivation
----------
The esim is moving towards modeling software as asynchronous processes, i.e. potentially concurrent processes. This is exemplified in the pex architecture where all components and interactions are always asynchronous.

However, asynchronous allows for a nasty class concurrency issues, e.g. race conditions or deadlocks, which are easy to make, hard to test, hard to reproduce, hard to debug, and hard to fix. Furthermore, concurrency issues are highly sensitive to timing and order of events and at the same time can have systemic effects when they arise. E.g. systems with concurrency issues can work perfectly for months until a subtle timing change in the environment can make take the entire system down.

Suggestion
----------
To mitigate the risks of concurrency issues, this Out of the Box project suggests that part of a solution could be to:

 1. For asynchronous processes, specify expected temporal relations between events (both input and outputs).
 2. (Automatically) test a process against the specifications by verifying that process event logs satisfy the specifications.
 3. (Automatically) derive relevant test sets with various event orderings and timings for processes from the specifications to obtain analyzable event logs.
 4. In aggregate system of communicating processes each with temporal specifications, (automatically) verify consistency between specifications.

Hypotheses
----------
The following hypotheses will be tested:

 1. A linear temporal logic is expressive enough to capture relevant temporal specifications.
 2. Event logs can be verified automatically against linear temporal logic formulas.
 3. It is possible to derive test sets semi-automatically from linear temporal logic formulas. 
 4. Linear temporal logic formals in aggregate can be verified for consistency.

Linear Temporal Logic
---------------------
Temporal logic is a large family of logics for formulating temporal relationships. A simple version is enhances predicate logic with the modalities **G**, **F**, **X** with the following meanings:

 - **G**_p_ - The formula _p_ always holds from now on.
 - **F**_p_ - The formula _p_ eventually holds.
 - **X**_p_ - The formula _p_ holds in the next time step.

For example, a formula that should hold for a simple asynchronous request-response structure is:

> **G**(Req(_i_) => **F**Res(_i_))

where Req(_i_) and Res(_i_) denotes the events that associated request and responses occurred (the _i_ encodes that the two events are related). This should be read as 

> "It is always the case that whenever Req(i) occurs then eventually Res(i) occurs."

It captures the notion that requests must always get a response. However the notion that that spurrious responses do not occur has not been captured _[TODO: I don't know how to express that]_.

The **X** modality is kind of weird, especially if time is taken as continuous. However it can be useful to express conditions where the current condition does not count in the conclusion. E.g. compare:

> **G**(P => **E**P)

> **G**(P => **XE**P)

The first expresses that if P happens at time _t_, P happens at some time ≥ _t_ which is trivially true (as the second P occurrence also can be the second occurrence). The second states that if P happens at time _t_, P happens at a time > _t_. That however is not trivial and will only hold if P never happens or repeats indefinetely.

Causal Event Association
-------------------------
For an event log, e.g.:

> [Request, Response, Request, Response]

it becomes important to know which event "belong together", i.e. that the first Response was caused by the first Request and similarly for the second.

Two ways to track Causal Event Association has been considered: Causal Identifiers or Functional Association.

### Causal Identifiers
For a request-response process, it may make sense to let the event carry a tracking id (or session id, ...), e.g. Request(117), where the requester generates a fresh id and the requestee promises to annotate the causally connected response with the same id, Response(117). E.g. an event log like:

> [Request(117), Request(3), Response(117), Response(3)]

This log is valid according to both id tracked liveliness and non-spurriousness:

> **G ALL**(_id_).(Request(_id_) => **F**Response(_id))

> **G ALL**(_id_).(Response(_id_) => **-F**Request(_id_))

### Functional Tracking
Causal Identifiers require the processes to carry ids which they might want to do. An alternative is Functional Tracking that associates events based on the event payload. E.g. for a request-response process that increments, an event log might look like:

> [Request(117), Response(118), Request(7), Response(8)]

This log is valid accoding to functional liveliness and non-spurriousness:

> **G ALL**(_n_, _n1_ where _n1_ = _n_ + 1).(Request(_n_) => **F**Respose(_n1_))

> **G ALL**(_n_, _n1_ where _n1_ = _n_ + 1).(Response(_n1_) => **-F**Request(_n_))



Results
-------
The following things were achieved at the Out-of-the-Box day March 23. 2018:

 1. A parser for a Linear Time Temporal Logic formulas with **G** (Will always be), **F** (Will eventually be), **X** (Next) modalities was written.
 2. A verifier of formulas on event logs was written.
 3. Request-response liveliness was formulated (**G**(Request => **F**Response)).
 4. The term language was expanded to be structured terms with unifyable variables.
 5. The expanded structured terms could capture event associations based on Causality Identifiers in some circumstances (**G**(Request(_id_) => **F**Response(_id_)).
 6. The parser was enhanced with reverse time modalities **-G** (Has always been), **-F** (Was at some point), **-X** (Previous).
 7. Spurrious Response Freeness was formulated (**G**(Response => **-F**Request)).
 8. Suggestions of freer placement of variable quantification was considered (**G ALL**(_id_).(Request(_id_) => **F**Response(_id_))).
 9. Suggestions of formulations of Functional Event Associations was considered (**G ALL**(_n_, _n1_ where Inc(_n_, _n1_))(RequestIncrement(_n_) => **F**ResponseIncrement(_n1_))).

Some interim conclusions.

 1. The formula **G**(Request => **F**Response) is a liveliness condition and expresses that requests must be responded.
 
 2. A condition stating that there are no spurrious responses (i.e. responses without prior requests) are harder to express. 

    This can easily be expressed by either time reversed **G**, **F**, e.g. **G**(Res(_i_) => **-F**Req(_i_)), modalities, or with a general time reversal operator, **R**, e.g. **R**(**G**(Res(_i_) => **F**Req(_i_))).

    As time reversal seems dual, it is possible that time formulas with time reversal can be transformed to non-time reversed formulas.

 3. Causality identifiers on events seem important to enable keep track of which events are related. However in a real life setting, there might not be suitable tracking ids. However relations may be able to be tracked on functional processes. E.g.: **G**(IncrementRequest(_n_) => **F**IncrementResponse(_n_ + 1)). More thought need to go into less functional processes.

 4. Forall is needed on variables: **G**(**ALL**(i).Res(_i_) => **F**Req(_i_))
 
 5. Functional relationships between events could be expressed as guards inside formulas: **G**(X(__x__) => **F**(Y(_y_) => **F**L(_l_) where IsLength(_l_, _x_, _y_)))