package;

using temporal.detail.Exp;
using temporal.detail.Unification;

class Main {
    static public function main() {
        var e1 = "Foo(x)".stringToExp();
        var e2 = "Foo(\"Hello\")".stringToExp();
        var u = Unification.empty();
        trace(u.unify(e1, e2));
        trace(e1.show());
        trace(e2.show());
        trace(u.follow(e1).show());
        trace(u.follow(e2).show());
    }
}