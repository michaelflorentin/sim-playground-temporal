package;

class LengthService {
    public function setX(x : Float) : Void {
        this.x = x;
        this.fire = true;
    }
    public function setY(y : Float) : Void {
        this.y = y;
        this.fire = true;
    }

    public function tick() : Void {
        if (fire) {
            fire = false;
            length(Math.sqrt(x * x + y * y));
        }
    }

    public var length : Float->Void = function(l) throw("unplugged");

    private var x : Float = 0;
    private var y : Float = 0;
    private var fire : Bool;
}

/*

G(SetX(x) | SetY(y) => F Length(l where IsLength(l, x, y))) 

*/