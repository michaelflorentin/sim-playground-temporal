package;

using simpleutil.MathUtils;
using stringskip.StringPos;
using temporal.detail.Exp;

class Main {
    static public function main() {

    }

    private function isLength(es : Array<ExpSource>, p : StringPos) : ExpSource {
        if (es.length == 3 && es[0].isFloat() && es[1].isFloat() && es[2].isFloat()) {
            var l = es[0].getFloat();
            var x = es[1].getFloat();
            var y = es[2].getFloat();
            return new ExpSource(Bool(
                (l * l).close(x * x + y * y)
            ), p);
        } else {
            logError(p, 'Illegal parameters to isLength.');
            return new ExpSource(Bool(false), p);
        }
    }

    static private function logError(p : StringPos, msg : String) : Void {
        trace('$p : $msg');
    }
    
}
