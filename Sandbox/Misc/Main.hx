package;

using simpleutil.IteratorUtils;
using stringskip.ParserUtils;
using stringskip.StringPos;
using temporal.detail.Exp;
using temporal.detail.Formula;
using temporal.detail.Parse;
using temporal.detail.Unification;
using temporal.detail.VerifyEventLog;

class Main {
    static public function main() {
        testRequestResponseLiveliness();
        testSpurriousResponse();
    }

    static public function testRequestResponseLiveliness() {

        verify("G(Req(i) => F Res(i))", false, 
            ["Req(A)"]
        );

        verify("G(Req(i) => F Res(i))", false, 
            ["Req(A)", "Res(B)"]
        );

        verify("G(Req(i) => F Res(i))", true,
            []
        );

        verify("G(Req(i) => F Res(i))", true, 
            ["Req(A)", "Res(A)"]
        );

        verify("G(Req(i) => F Res(i))", true, 
            ["Req(A)", "Req(B)", "Res(B)", "Res(A)"]
        );

        verify("G(Req(i) => F Res(i))", true, 
            ["Req(A)", "Req(B)", "Res(A)", "Res(B)"]
        );

        verify("G(Req(i) => F Res(i))", true, 
            ["Req(A)", "Res(A)", "Req(B)", "Res(B)"]
        );

        // spurrious responses are fine for liveliness.
        verify("G(Req(i) => F Res(i))", true, 
            ["Req(A)", "Res(B)", "Res(A)"]
        );

        // This demonstrates that the quantification of i is at the wrong 
        // place.
        /*verify("G(Req(i) => F Res(i))", false, 
            ["Req(A)", "Res(A)", "Req(B)", "Res(C)"]
        );*/
    }

    static public function testSpurriousResponse() {
        verify("G(Res(i) => -F Req(i))", true, 
            ["Req(A)", "Res(A)"]
        );
        verify("G(Res(i) => -F Req(i))", false, 
            ["Res(A)"]
        );
        verify("G(Res(i) => -F Req(i))", false, 
            ["Req(A)", "Res(B)", "Res(A)"]
        );
    }

    static public function verify(
        p : String, expectValidate : Bool, 
        ws : Array<String>
    ) : Void {
        var log = ws.stringsToExps();
        var e = p.doHxTrimParseTrace("p", Parse.parseFormula);
        var u = Unification.empty();
        var funs = function(n : String) return null;
        var logError = function(p : StringPos, msg : String) trace('$p : $msg');
        var validate = e.verify([], log, u, funs, logError);
        var expected = expectValidate == validate;
        var log2 = log.showElements(ExpUtils.showDeepC(u), ", ");
        trace('[${log2}]');
        trace('> ${e.show()}: ${if (validate) "valid" else "not valid"} ${if (expected) "as expected" else "unexpected!"} ');
    }

}
